% SURF
feature=zeros(129,122,64);
performance=zeros(129,5); %image_no % no. of feature points extracted 1 2 %matchpoints 
for i=10:129
    
    image_def='C:\Users\Siddhant Jain\Desktop\BTP\Feature extractors\Test Data\1 (4).jpg';
    image_loc='C:\Users\Siddhant Jain\Desktop\BTP\Feature extractors\Test Data\1 (';
    back=').jpg';
    image_loc=strcat(image_loc,int2str(i),back);
    I1=imread(image_def);
    I1=rgb2gray(I1);
    %imshow(I1)
    tic
    points = detectSURFFeatures(I1);
    [features1, valid_points1] = extractFeatures(I1,points);
% %     imshow(I);
% %     hold on;
% %     strongestPoints = valid_points.selectStrongest(30);
% %     strongestPoints.plot('showOrientation',true);
% %     strongestPoints = valid_points;
% %     imshow(I);
% %     hold on;
% %     strongestPoints.plot('showOrientation',true);
    I2=imread(image_loc);
% %     I2=rgb2gray(I2);
    %imshow(I1)
    tic
    points = detectSURFFeatures(I2);
    [features2, valid_points2] = extractFeatures(I1,points);
    indexPairs = matchFeatures(features,features2);
    matchedPoints1 = valid_points1(indexPairs(:,1),:);
    matchedPoints2 = valid_points2(indexPairs(:,2),:);
    toc
    
    figure(1); showMatchedFeatures(I1,I2,matchedPoints1,matchedPoints2);
    sizes=size(features1);
    performance(i,1)=i;
    performance(i,2)=sizes(1,1);
    sizes=size(features2);
    performance(i,3)=sizes(1,1);
    sizes=size(indexPairs);
    performance(i,4)=sizes(1,1);
    a=1;
    
    
end
