%% This Script takes as input the stroke data from a wacom tablet and creates a final gif with the character.

clear; clc;

top_dir=dir('.');
dirFlags=[top_dir.isdir];
subFolders = top_dir(dirFlags);
for i=7:38
    
list=dir(subFolders(i).name);

for j=3:52
c_file=[subFolders(i).name '/' list(j).name];
fileID=fopen(c_file);
formatSpec='%f,%f';
% A = fscanf(fileID,formatSpec)
C = textscan(fileID,formatSpec);
rawdata=[C{1,1} C{1,2}];

I=zeros(500,500);
I(:,:)=1;
si=size(rawdata);
for k=1:si(1)
    
    if rawdata(k,1)>0 && rawdata(k,2)>0
        
    I(rawdata(k,1),rawdata(k,2))=0;
   
    end 
    
end

fclose(fileID);
se = strel('disk',6);
se1 = strel('disk',3);
I = imerode(I,se1);
I=imopen(I,se);
I=flip(I,2);
I=rot90(I);
X=imcomplement(I);
CH = bwconvhull(X);
r=regionprops(CH);
IMG=imcrop(I,[round(r.BoundingBox(1)) round(r.BoundingBox(2)) round(r.BoundingBox(3)) round(r.BoundingBox(4))]);
IMG=imresize(IMG,[200 250]);
filename=[subFolders(i).name '/' subFolders(i).name '_' int2str(j) '.png'];
imwrite(IMG,filename)

end
a=1;

end