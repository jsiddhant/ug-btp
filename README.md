# Statistical Language Modelling for Image Classification. 

## Into:

The project uses n-gram language modelling for image classification due to its ability to model relative relations in sequences of 'n' features. We use the CMU SLM toolkit (Readme included) for language modelling functions.

## Note: This repo only contains the partial code for the project. Following is missing:

* Code to generate SLM Input from clustered features. 
* Python Scripts to generate category wise n-gram models using SLM toolkit
* Testing Scripts.

