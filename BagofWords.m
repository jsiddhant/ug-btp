%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
rootFolder='C:\Users\Siddhant Jain\Desktop\BTP\Sem1\101_ObjectCategories';
imgSets = [ imageSet(fullfile(rootFolder, 'airplanes')), ...
            imageSet(fullfile(rootFolder, 'ferry')), ...
            imageSet(fullfile(rootFolder, 'laptop')) ];
minSetCount = min([imgSets.Count]);
imgSets = partition(imgSets, minSetCount, 'randomize');
[trainingSets, validationSets] = partition(imgSets, 0.3, 'randomize');
bag = bagOfFeatures(trainingSets);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
imageIndex = invertedImageIndex(bag);
addImages(imageIndex, imgSets(1));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

imgs=imread(imageIndex.ImageLocation{1,1});
imshow(imgs)
hold on
scatter(imageIndex.ImageWords(1,1).Location(:,1),imageIndex.ImageWords(1,1).Location(:,2))