
f=fullfile('C:\Users\Siddhant Jain\Desktop\BTP\self\greek_letters');
greek_data_complete = imageSet(f,'recursive');
%tbl = countEachLabel(devnagri_data_complete);
%[trainingSet, validationSet] = splitEachLabel(devnagri_data_complete, 0.7, 'randomize');
[trainingSet, validationSet] = partition(greek_data_complete, 0.5, 'randomized');

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% airplanes = find(trainingSet.Labels == 'a', 1);
% ferry = find(trainingSet.Labels == 'cha', 1);
% laptop = find(trainingSet.Labels == 'ja', 1);
% 
% % figure
% 
% subplot(1,3,1);
% imshow(readimage(trainingSet,airplanes))
% subplot(1,3,2);
% imshow(readimage(trainingSet,ferry))
% subplot(1,3,3);
% imshow(readimage(trainingSet,laptop))
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

bag=bagOfFeatures(trainingSet); 
categoryClassifier = trainImageCategoryClassifier(trainingSet, bag);
confMatrix = evaluate(categoryClassifier, trainingSet);
confMatrix1 = evaluate(categoryClassifier, validationSet);
mean(diag(confMatrix1));



% http://in.mathworks.com/help/vision/examples/image-category-classification-using-bag-of-features.html